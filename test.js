import http from 'k6/http';
import { check } from 'k6';

// K6 runs this script, and provides environment URLs through __ENV instead of process.env
const SITE_URL = __ENV.ENVIRONMENT_URL || 'http://localhost:3000';

export default function () {
    const rnd = Math.floor(Math.random() * 1000);
    const response = http.get(`${SITE_URL}/${rnd}`);
    check(response, {
        "is status 200": (r) => r.status === 200,
        "is id the same we asked": (r) => {
            const id = Number(r.json("id"))
            return id === rnd
        }
    })
}

export let options = {
    vus: 1,
    duration: '5s',
};
