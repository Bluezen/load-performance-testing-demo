const app = require('express')();

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

app.get("/:id", async (req, res) => {
    const id = req.params.id;
    // respond slowly
    await sleep(50);
    // randomly return the wrong number 10% of the time
    if (Math.random() < 0.1) {
        res.json({
            id: 108
        });
    }
    res.json({
        id
    });
});

app.listen(3000);